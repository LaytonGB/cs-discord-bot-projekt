using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace CS_The_Quartermaster
{
    public class Commands : ModuleBase<SocketCommandContext>
    {
        [Command("ping")]
        public async Task Ping()
        {
            string pong = "Pong";
            var t1 = DateTime.Now;
            IUserMessage userMessage = await Task.FromResult(ReplyAsync(pong)).Result;
            var t2 = DateTime.Now;
            await userMessage.ModifyAsync((userMessage) => {
                userMessage.Content = pong + "\n" + (t2-t1).ToString() + " ms";
            });
        }
    }
}
